package com.example.shop.service;

import com.example.shop.models.Product;
import com.example.shop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.shop.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepositry;

    //List all products
    public List<Product> getAllProduct() {
        List<Product> productList = productRepositry.findAll();
        if (productList.size() > 0) {
            return productList;
        } else {
            return new ArrayList<Product>();
        }
    }

    //Create a product
    public Product createProduct(Product product) {
        return productRepositry.save(product);
    }

    //Get product by specific id
    public Product getProductById(Long id) throws ResourceNotFoundException {
        Optional<Product> product = productRepositry.findById(id);
        if (product.isPresent()) {
            return product.get();
        } else {
            throw new ResourceNotFoundException("No product record exist for given id", id);
        }
    }

    //Update - upsert
    public Product createOrUpdateProduct(Product newProduct) throws ResourceNotFoundException {
        if (newProduct.getId() != null) {
            Optional<Product> product = productRepositry.findById(newProduct.getId());
            if (product.isPresent()) {
                Product updatedProduct = product.get();
                updatedProduct.setProductName(newProduct.getProductName());
                updatedProduct.setYear(newProduct.getYear());
                updatedProduct.setPrice(newProduct.getPrice());
                updatedProduct.setUrl(newProduct.getUrl());
                updatedProduct = productRepositry.save(updatedProduct);
                return updatedProduct;
            } else {
                newProduct = productRepositry.save(newProduct);
                return newProduct;
            }
        } else {
            newProduct = productRepositry.save(newProduct);
            return newProduct;
        }
    }

    //Delete product by specific id
    public void deleteProductById(Long id) throws ResourceNotFoundException {
        Optional<Product> product = productRepositry.findById(id);
        if (product.isPresent()) {
            productRepositry.deleteById(id);
        } else {
            throw new ResourceNotFoundException("No product record exist for given id", id);
        }
    }


}
