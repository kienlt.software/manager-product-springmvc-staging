package com.example.shop.controller;

import com.example.shop.models.dto.UserRegistrationDto;
import com.example.shop.service.UserServiceRegistration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/register")
public class UserRegistrationController {

    private UserServiceRegistration userServiceRegistration;

    public UserRegistrationController(UserServiceRegistration userServiceRegistration) {
        super();
        this.userServiceRegistration = userServiceRegistration;
    }

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm() {
        return "register";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") UserRegistrationDto registrationDto) {
        userServiceRegistration.save(registrationDto);
        return "redirect:/register?success";
    }

}
